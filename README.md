# cof_s2

Petites pancartes personnalisées avec les noms des coffeu.se.s pour l'AGB du S2

La police est "GOLDLEAF BOLD" en taille 54, on a essayé de mettre un espacement
de 2.5 entre les lettres pour que la cnc ne détruise pas tout.

## Peinture

1. Violet et jaune et bleu dans le fond
2. Jaune sur le dessus
3. Retouche en violet/bleu de jaune de l'étape 2 qui a débordé

Si on est motivé on met des couleurs différentes sur la palette bda
